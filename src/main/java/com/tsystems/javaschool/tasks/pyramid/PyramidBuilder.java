package com.tsystems.javaschool.tasks.pyramid;

import java.util.ArrayList;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {

        if (inputNumbers == null || inputNumbers.contains(null)){
            throw new CannotBuildPyramidException();
        }

        int size = inputNumbers.size();
        int indexOfTriangleNumber = getIndexOfTriangleNumber(size);

        inputNumbers.sort((o1, o2) -> {
            if (o1 > o2)
                return 1;
            else if(o2 > o1)
                return -1;
            return 0;
        });

        return createArray(inputNumbers, indexOfTriangleNumber);
    }

    /**
     * Returns position in triangle numbers sequence if number is triangle or
     * throw CannotBuildPyramidException.
     * @param number size of the List inputNumbers
     * @return number's position in triangle numbers sequence
     */
    private int getIndexOfTriangleNumber(int number){

        if (number == 1){
            return 1;
        }

        for (int i = 1; i < number; i++) {
            if(number == i * (i+1) /2){
                return i;
            }
        }

        throw new CannotBuildPyramidException();
    }

    /**
     * Creates an array and fills it with zeros and numbers from inputNumbers List in the shape of pyramid.
     * @param inputNumbers to be used in the pyramid
     * @param indexOfTriangleNumber to indicate number of rows and columns of the array
     * @return 2d array with pyramid inside
     */
    private int[][] createArray(List<Integer> inputNumbers, int indexOfTriangleNumber){

        int[][] result = new int[indexOfTriangleNumber][indexOfTriangleNumber*2-1];
        int pointer = 0;

        // For every line of the array result
        for (int i = 0; i < indexOfTriangleNumber; i++) {

            // Creates subList with elements for line i
            List<Integer> subList = new ArrayList<>();
            int numbersCount = i + 1;
            for (int j = pointer; j < pointer + numbersCount; j++) {
                subList.add(inputNumbers.get(j));
            }
            pointer += numbersCount;

            // Adds 0's before numbers from subList
            int countPosition = -1;
            if(indexOfTriangleNumber - i - 1 > 0) {
                for (int k = 0; k < indexOfTriangleNumber - i - 1; k++) {
                    result[i][++countPosition] = 0;
                }
            }

            // Adds numbers from subList separated by 0's
            for (int k = 0; k < i + 1; k++) {

                if(k < i){
                    result[i][++countPosition] = subList.get(0);
                    subList.remove(0);
                    result[i][++countPosition] = 0;
                }

                else{
                    result[i][++countPosition] = subList.get(0);
                    subList.remove(0);
                }
            }

            // Adds 0's after numbers from subList
            if(indexOfTriangleNumber - i - 1 > 0) {
                for (int k = 0; k < indexOfTriangleNumber - i - 1; k++) {
                    result[i][++countPosition] = 0;
                }
            }

        }

        return result;
    }

}
