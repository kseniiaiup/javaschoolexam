package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.util.LinkedList;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {

        if(statement == null || !check(statement)){
            return null;
        }

        String operators = "-+/*";
        String brackets = "()";
        String digits = "\\d+.?+\\d*+";

        Stack<String> numbers = new Stack<>();
        Stack<String> symbols = new Stack<>();

        String prev = "";
        String next;

        int bracketsCount = 0;
        statement = statement.replaceAll(" ", "");

        StringTokenizer tokenizer = new StringTokenizer(statement, brackets+operators, true);
        while (tokenizer.hasMoreElements()){
            next = tokenizer.nextToken();

            // If next element is a number
            if (next.matches(digits)){
                if(prev.matches(digits)){
                    return null;
                }

                numbers.push(next);
                prev = next;

                if(!tokenizer.hasMoreElements()){
                    while (!(numbers.size()==1)){
                        compute(symbols.pop(), numbers);
                    }
                }

            }

            // If next element is an operator
            else if(next.matches("[-+*/]")){
                if(numbers.isEmpty() || prev.matches("[-+*/]") || prev.equals("(")){
                    return null;
                }

                String element = "";

                if(!symbols.isEmpty()){
                    element = symbols.peek();
                }

                if(priority(next) > priority(element)){
                    symbols.push(next);
                    prev = next;
                }

                else{
                    while (priority(next) <= priority(element)) {
                        symbols.pop();
                        compute(element, numbers);
                        if(!symbols.isEmpty()) element = symbols.peek();
                        else element = "";
                    }

                    symbols.push(next);
                    prev = next;
                }
            }

            // If next element is a bracket
            else if(next.matches("[()]")){
                if(next.equals("(")){
                    bracketsCount++;
                    symbols.push(next);
                    prev = next;
                }
                else if(next.equals(")")){
                    if (bracketsCount == 0 || prev.matches("[-+*/]") || prev.equals("(")){
                        return null;
                    }

                    String element = symbols.peek();

                    while (!element.equals("(")){
                        symbols.pop();
                        compute(element, numbers);
                        element = symbols.peek();
                    }

                    symbols.pop();
                    prev = numbers.peek();
                    bracketsCount--;
                    if(!tokenizer.hasMoreElements()){
                        while (!(numbers.size()==1)){
                            compute(symbols.pop(), numbers);
                        }
                    }
                }
            }

            // If element is another symbol
            else return null;
        }

        if(numbers.size()==1 && bracketsCount==0) {
            String temp = numbers.pop();

            if(temp.matches("-?\\d+.?+0+")){
                Double result = Double.parseDouble(temp);
                return String.valueOf(result.intValue());
            }

            Double result = Double.parseDouble(temp);

            if(result.isInfinite() || result.isNaN()){
                return null;
            }

            DecimalFormat format = new DecimalFormat();
            format.setMaximumFractionDigits(4);
            format.format(result);

            return result.toString();
        }

        return null;
    }

    /**
     * Checks if statement contains only available symbols.
     * @param statement String to check
     * @return true if statement contains only available symbols
     */
    private boolean check(String statement){
        String regex = "[()+\\-/*0-9.]*";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(statement);

        return matcher.matches();
    }

    /**
     * Returns a priority for a symbol.
     * @param s is a symbol to check
     * @return priority for a symbol
     */
    private int priority(String s){
        if(s.matches("[()]")) return 1;
        if(s.matches("[-+]")) return 2;
        if(s.matches("[*/]")) return 3;

        return 0;
    }


    /**
     * Performs an arithmetic operation on two first numbers from the numbers Stack and pushes the result
     * to the numbers Stack.
     * @param operator for an arithmetic operation
     * @param numbers Stack of numbers to calculate
     */

    private void compute(String operator, Stack<String> numbers){
        double second = Double.parseDouble(numbers.pop()),
                first = Double.parseDouble(numbers.pop()),
                result = 0;

        if(operator.equals("+")) result = first + second;
        if(operator.equals("-")) result = first - second;
        if(operator.equals("*")) result = first * second;
        if(operator.equals("/")) result = first / second;

        numbers.push(String.valueOf(result));
    }

    /**
     * The <code>Stack</code> class represents a last-in-first-out
     * (LIFO) stack of objects. It extends class <tt>LinkedList</tt>.
     * <p>
     * When a stack is first created, it contains no items.
     */
    private static class Stack<E> {
        private LinkedList<E> list = new LinkedList<>();

        void push(E element){
            list.addFirst(element);
        }

        E pop(){
            return list.removeFirst();
        }

        E peek(){
            return list.getFirst();
        }

        int size() {
            return list.size();
        }

        boolean isEmpty(){
            return list.isEmpty();
        }
    }
}
